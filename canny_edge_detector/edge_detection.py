import sys
import cv2
import math
import itertools
import numpy as np
from scipy.signal import convolve2d
from matplotlib import pyplot as plt
from sklearn.metrics.pairwise import euclidean_distances

def gaussianKernel2D(size, sigma):
	"""Compute and return a 2D Gaussian kernel of the given size and sigma.

	Paramters:
	- size -- the size of the desired kernel.  
	- sigma -- the standard deviation used to compute the values of the kernel

	Returns: 
	- k -- a size x size Gaussian kernel with standard deviation sigma
	"""

	k = cv2.getGaussianKernel(size, sigma)
	return convolve2d(k,k.T).astype(np.float32)


def max_format(a1, a2):
	"""Given 2 ndarrays a1 and a2, return the data type with the wider range"""
	try:
		t1_max = np.iinfo(a1.dtype).max
	except ValueError:
		t1_max = np.finfo(a1.dtype).max
	try:
		t2_max = np.iinfo(a2.dtype).max
	except ValueError:
		t2_max = np.finfo(a2.dtype).max

	return a1.dtype if t1_max > t2_max else a2.dtype


def convolve(image, kernel):
	"""Apply the given kernel to the given image.  Image file and kernel should
	be 8-bit unsigned, 16-bit unsigned, or 32-bit floating point.  Zero-padding
	is applied at the edges.  

	Parameters:  
	- image -- an n x m ndarray representing a grayscale image  
	- kernel -- an x x y ndarray representing the kernel to apply  

	Returns: 
	- out_img - an n x m ndarray representing the image with the kernel applied.
	"""
	# flip kernel
	kernel = kernel[::-1, ::-1]

	# Promote differing formats
	fmt = max_format(image, kernel)
	if image.dtype != fmt:
		image = image.astype(fmt)
	else:
		kernel = kernel.astype(fmt)

	# Create the output image with same size of image
	out_img = np.zeros(image.shape, fmt)
	
	# For every pixel in the image
	for row in range(image.shape[0]):
		for px in range(image.shape[1]):
			# Create the new pixel value
			new_px = 0

			# Get the radius[sic] of the kernel
			radius = kernel.shape[0] // 2

			# For every pixel in the kernel
			for k_row in range(kernel.shape[0]):
				for k_px in range(kernel.shape[1]):
					# Get the corresponding pixel coordinates
					c_row = row + (k_row - radius)
					c_px = px + (k_px - radius)

					# If the corresponding coordinates are out of image bounds, zero
					if not(0 <= c_row < image.shape[0] \
							and 0 <= c_px < image.shape[1]):
						new_px += 0

					# Otherwise multiply the kernel and pixel value and add to new pixel
					else:
						c_px_val = image[c_row, c_px]
						k_px_val = kernel[k_row, k_px]
						new_px += kernel[k_row, k_px] * image[c_row, c_px]

			# Set the pixel in the new image to the new pixel
			out_img[row, px] = new_px

	# Return the new image
	return out_img



def calc_gradient(image):
	"""Calculate the gradient magnitudes and directions for the given image. 

	Parameters: 
	- image -- an n x m array representing a greyscale image

	Returns: 
	- rs -- an n x m array of the gradient magnitudes for each pixel of	image  
	- thetas -- an n x m array of the gradient angles for each pixel of	image, 
	in radians
	"""

	# Get the Sorbel Partials
	# edit: Scharr operator plays nicer
	# edit: no it doesn't
	x_sobel = np.array([
		[-1, 0, 1],
		[-2, 0, 2],
		[-1, 0, 1]
	])

	x_partial = convolve(image, x_sobel[::-1,::-1])
	y_partial = convolve(image, x_sobel.T[::-1,::-1])

	# Use trigonometry to convert from cartesian to polar values
	rs = np.sqrt(x_partial ** 2 + y_partial ** 2)
	thetas = np.arctan2(y_partial, x_partial)

	# Write gradient magnitude to image
	cv2.imwrite('%s_mag.png'%sys.argv[1], rs)

	# Show the gradients and partials
	plt.subplot(221),plt.imshow(x_partial, cmap='gray'),plt.title('SobelX')
	plt.xticks([]), plt.yticks([])
	plt.subplot(222),plt.imshow(y_partial, cmap='gray'),plt.title('SobelY')
	plt.xticks([]), plt.yticks([])
	plt.subplot(223),plt.imshow(rs, cmap='gray'),plt.title('Grad_mags')
	plt.xticks([]), plt.yticks([])
	plt.subplot(224),plt.imshow(thetas, cmap='gray'),plt.title('Grad_directs')
	plt.xticks([]), plt.yticks([])
	plt.show()

	return (rs, thetas)



def nonmax_supression(g_mags, g_dirs):
	"""Examine the given sets of gradient magnitudes and directions and locate
	the maximum to filter out any potential edges.

	Parameters: 
	- g_mags -- an n x m matrix of gradient magnitudes for each pixel in the 
	source image. 
	- g_dirs -- an n x m matrix of angles (in radians) representing the 
	direction of the gradient. 

	Returns: 
	- an n x m matrix of potential edges within the images.
	"""
	# Create the output edge matrix
	edges = np.zeros(g_mags.shape, np.uint8)

	# Iterate over the magnitude matrix
	for row in range(g_mags.shape[0]):
		for px in range(g_mags.shape[1]):
			# Get the angle of the direction, converting to ° for simplicity.
			# Modulo it by 180 to avoid need to check two ranges for each dir
			#theta = g_dirs[row,px] / 180 * math.pi % 180
			theta = round(g_dirs[row,px] / (math.pi/4)) % 4

			# Check direction
			# WE
			if theta == 0:
				check1 = (row+0, px-1)
				check2 = (row+0, px+1) 
			# NWSE
			elif theta == 1:
				check1 = (row-1, px-1) 
				check2 = (row+1, px+1) 
			# NS
			elif theta == 2:
				check1 = (row-1, px+0)
				check2 = (row+1, px+0) 
			# NESW
			elif theta == 3:
				check1 = (row-1, px+1)
				check2 = (row+1, px-1) 

			# Get the relevant magnitude values.  If the coordinates for the 
			# neighbours fall outside of the image bounds, set them to -infinity
			mag = g_mags[row,px]
			if 0 <= check1[0] < g_mags.shape[0] \
				and 0 <= check1[1] < g_mags.shape[1]:
				mag1 = g_mags[check1]
			else:
				mag1 = -np.inf
			if 0 <= check2[0] < g_mags.shape[0] \
				and 0 <= check2[1] < g_mags.shape[1]:
				mag2 = g_mags[check2]
			else:
				mag2 = -np.inf 

			# If this is a maximum, mark it as a potential edge (127=potential)
			if mag > max(mag1,mag2):
				edges[row,px] = 127
	
	# return matrix of potential edges
	return edges



def double_threshold(edges, g_mags, weak, strong):
	"""Filter out any edges whose magnitudes fall below the given weak threshold
	and mark any edges which are above the strong threshold as definite edges.
	This function modifies the edges matrix in place.

	Parameters: 
	- edges -- an n x m matrix of potential edges.  
	- g_mags -- an n x m matrix of gradient magnitudes for each pixel of the 
	source image 
	- weak -- the threshold separating definite non-edges from potential edges
	- strong -- the threshold separating definite edges from potential edges
	"""
	# Iterate over the magnitude matrix
	for row in range(g_mags.shape[0]):
		for px in range(g_mags.shape[1]):
			# Skip non-potential edges
			if edges[row,px] != 127:
				continue
			
			# Get the magnitude
			mag = g_mags[row,px]

			# Determine edge certainty based on strong and weak threshold
			if mag >= strong:
				edges[row,px] = 255
			elif mag < weak:
				edges[row,px] = 0



def hysterize(edges):
	"""Examine all remaining potential edges, using blob analysis to determine
	if they are a true edge or not based on connectivity to another true edge.  
	Modifies the edges matrix in place.
	"""
	# Create a copy of the array to keep track of changes
	edges_old = np.array(edges, copy=True)

	# Create a flag to track if array is unchanged
	unchanged = False

	# Loop until no potential edges remain
	while 127 in edges:
		# For each pixel in the image
		for row in range(edges.shape[0]):
			for px in range(edges.shape[1]):
				# Skip if the pixel is not a potential edge (done this way to 
				# avoid too many indents)
				if edges[row,px] != 127:
					continue
				
				# Suppress to zero if unchanged is true
				if unchanged:
					edges[row,px] = 0
					continue
				
				# Iterate over neighbors
				coords = itertools.product([row+1,row,row-1],[px+1,px,px-1])
				for c in coords:
					# Get the value of this neighbor
					if 0 <= c[0] < edges.shape[0] \
						and 0 <= c[1] < edges.shape[1]:
						neighbor_val = edges[c]
					
						# If the neighbor is a sure edge, mark this point as
						# a sure edge and break out of the loop
						if neighbor_val == 255:
							edges[row,px] = 255
							break
		
		# Check if image is unchanged.  If so, all remaining 127's will be 
		# suppressed.
		if np.allclose(edges, edges_old):
			unchanged = True

		# Otherwise copy the current edges to edges_old
		else:
			edges_old = np.array(edges, copy=True)



def canny(image, scale, weak, strong):
	"""Implementation of the Canny Edge Detector.  Takes an image and blurring
	scale, as well as an upper and lower threshold for edge determination.

	Parameters: 
	- image -- the image to perform edge detection on
	- scale -- the size of the Gaussian kernel to use for blurring
	- weak -- the threshold separating definite non-edges from potential edges
	- strong -- the threshold separating definite edges from potential edges
	"""
	# 1. Noise Reduction
	# Note to self: scale = sigma
	# Note to self: consider implementing own gaussian kernel function
	kernel = gaussianKernel2D(scale, 0.45)
	image = convolve(image, kernel[::-1,::-1])
	
	# 2. Gradients
	g_mags, g_dirs = calc_gradient(image)

	# 3. NonMax Suppression
	# Get potential edges
	edges = nonmax_supression(g_mags, g_dirs)

	# 4. Double Thresholding
	# Filter out definite non edges and determine definite strong edges
	double_threshold(edges,g_mags,weak,strong)

	# 5. Hysteresis
	# Hysterize the data
	hysterize(edges)

	# Return the edges
	return edges



def main(img_path):
	"""Test function for rit_conv()"""
	# Read in image from command line
	img = cv2.imread(img_path)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	# Run the edge detector
	e = canny(img, 5, 50, 150)

	# Write the edges to file
	cv2.imwrite('%s_edges.png'%img_path, e)
	
	# Show the images side by side
	plt.subplot(121),plt.imshow(img, cmap='gray'),plt.title('Original')
	plt.xticks([]), plt.yticks([])
	plt.subplot(122),plt.imshow(e, cmap='gray'),plt.title('my_edges')
	plt.xticks([]), plt.yticks([])
	plt.show()



if __name__ == '__main__':
	main(sys.argv[1])
