# Samuël Hobson
# 2019.10.19
#
# Script which generates the next element in a Conway-like sequence
# This script is dedicated to my boy Jig

import sys

def get_next_line(line):
	new_line = []
	
	# Target is the number we're counting
	target = line[0]
	count = 0

	for num in line:
		if num == target:
			count += 1
		else:
			# add count and target to new line, set target to new number,
			# and reset count to 1
			new_line.extend([count, target])
			target = num 
			count = 1

	# add the final count and target to the list, then return
	new_line.extend([count, target])

	return new_line


def main():
	# convert command line inputs to integers
	line = [int(i) for i in sys.argv[1:]]
	print(get_next_line(line))


if __name__ == '__main__':
	if len(sys.argv) > 1:
		main()
	else:
		print("Please input a sequence of numbers separated by a space")